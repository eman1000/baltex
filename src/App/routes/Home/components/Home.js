import React, { Component } from "react";
import Page from "../../../components/Page";
import "./HomeStyles.css";
import About from "./About";

class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // showDevApp:false
    };
  }
  componentDidMount(){
    console.log(this.props.translations);
  }
  render() {
    return (
      <Page
        id="homepage"
        title="Baltex ICO"
      >
        <About/>
      </Page>
    );
  }
}

export default HomePage;