import React from "react";
import { defineMessages, injectIntl, intlShape } from "react-intl";


const defaultMessages = defineMessages({
  greeting: {
    id: "app.home.greeting",
    description: "Message to greet the user.",
    defaultMessage: "Hie, {name}!"
  }
});
const About = ({intl})=>{

  const { formatMessage } = intl;

  return (
    <section id="about-us">
      <div className="about-wrapper">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              { formatMessage(defaultMessages.greeting, {name:"Eman"}) }
            </div>
            <div className="col-md-6">
              frfefef
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

About.propTypes = {
  intl   : intlShape.isRequired
};
export default injectIntl(About);
