import { connect } from "react-redux";
import Home from "../components/Home";

import {
} from "../module/home";

const mapStateToProps = (state) => ({
  locale: state.intl.locale,
  translations: state.intl.translations
});

const mapDispatchToProps = {
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);