import { combineReducers } from "redux";
import {intlReducer} from "react-intl-redux";

import login from "../App/routes/Login/module";
import home from "../App/routes/Home/module/home";

export default combineReducers({
  login,
  home,
  intl: intlReducer
});
